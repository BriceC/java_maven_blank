package fr.skillstesting.blankapp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BlankAppTest {

    @Test
    public void should_return_true() {
        Boolean ret = BlankApp.doSomething();
        Assertions.assertEquals(false, ret);
    }
}
